package uk.ac.reading.student.zj018597.whatstheplan.model;

/**
 * Model of a plan
 */
public interface Item {
    int getId();
    String getName();
}
